<?php 
header("Content-Type: application/json");
if($_SERVER["REQUEST_METHOD"] === "OPTIONS") {
	//FOR CORS
	http_response_code(204);
	die("");
}
// if($_SERVER["REQUEST_METHOD"]!=="GET"&&$_SERVER["REQUEST_METHOD"]!=="POST"&&$_SERVER["REQUEST_METHOD"]!=="PUT") {
// 	return_error("Invalid request method, use GET or POST instead.");
// }
$data = null;
if($_SERVER["REQUEST_METHOD"]==="POST") {
	if($_SERVER["CONTENT_TYPE"]!=="application/json") {
		return_error("Invalid content type, use application/json instead.");
	}
	$data = json_decode(file_get_contents("php://input"),true);
	if($data == null) {
		return_error("Invalid JSON provided. Can't parse input.");
	}
}
if($_SERVER["REQUEST_METHOD"]==="PUT") {
	if($_SERVER["CONTENT_TYPE"]!=="application/json") {
		return_error("Invalid content type, use application/json instead.");
	}
	$data = json_decode(file_get_contents("php://input"),true);
	if($data == null) {
		return_error("Invalid JSON provided. Can't parse input.");
	}
}
if($_SERVER["REQUEST_METHOD"]==="GET") {
	$data = $_GET;
	if($data == null) {
		return_error("Invalid data provided. Can't parse input.");
	}
}
if($_SERVER["REQUEST_METHOD"]==="DELETE") {
	$data = $_GET;
}



function return_error($description,...$args) {
	http_response_code($args[0]??400);
	echo(json_encode(array("state" => "error", "description"=>$description)));
	if($args[0]??400 >=500) {
		throw new Exception($description);
	}
	die();
}
function return_data($data_array,...$args) {
	if(!is_array($data_array)) {
		return_error("Invalid data-type supplied to return_data() function.",500);
	}
	http_response_code($args[0]??200);
	$data_array["state"] = "success";

	die(json_encode($data_array));
}
function return_success(...$args) {
	http_response_code($args[0]??200);
	$data_array["state"] = "success";
	die(json_encode($data_array));
}
 ?>