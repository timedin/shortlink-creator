<?php 
require_once(__DIR__."/../../res/json-api-common.php");
require_once(__DIR__."/../../res/setup.php");

$vt_api_endpoint = "https://www.virustotal.com/api/v3/domains/";


if(!isset($data["url"])) return_error("No URL provided!");
$url 	= $data["url"];
$protected= ($data["protected"]??false);
$tiny	= ($data["tiny"]??false);
$usages	= intval($data["usages"]??0);

//URL CHECK
if(!str_starts_with($url, "http://") && !str_starts_with($url, "https://")) return_error("Invalid URL provided!");
if(str_contains($url, " ")) return_error("Invalid URL provided!");
$domain = explode("/", $url)[2];
if(strpos($domain, ".")==0||strrpos($domain,".")<1 || strrpos($domain,".")>=strlen($domain)-2) return_error("Invalid URL provided!");

if($usages <0) {
	return_error("Usages needs to be higher than or 0!");
}

if(isset($data["expire_date"])) {
	$expiryParsed = strtotime($data["expire_date"]);
	if($expiryParsed === FALSE || $expiryParsed <= strtotime('now')) return_error("Invalid date provided!");
	$expiryDate = $data["expire_date"];
} else {
	$expiryDate = null;
}
//VT-SCAN
if($vt_config["enabled"] === true) {
	$domain = explode("/", $url)[2];
	if(strpos($domain, ".")==0||strrpos($domain,".")<1 || strrpos($domain,".")>=strlen($domain)-2) return_error("Invalid URL provided!");

	$vt_array=curl($vt_api_endpoint.$domain,$vt_config["token"]);
	$sources = $vt_array["data"]["attributes"]["last_analysis_results"];
	$occurences = 0;
	foreach ($sources as $source => $value) {
		if($value["result"]!=="clean" && $value["result"] !=="unrated") {
			$occurences++;
		}
	}
	if($occurences >$vt_config["threshold"]) {
		return_error("URL provided could be infected!");
	}
}
try {

	$key = createKey($tiny?$config["tiny-length"]:$config["length"]);

	$sql = "SELECT `id` FROM `entries` WHERE `id`=\"%a0\";";
	for ($i=0; ($i < 12 && ($DBManager->query($sql,$key)!=null)); $i++) { 
		$key = createKey($tiny?$config["tiny-length"]:$config["length"]);
	}
	if($expiryDate != null) {
		$sql = "INSERT INTO `entries` (`id`, `url`, `protected`, `valid_until`, `usages`) VALUES ('%a0', '%a1', ".($protected?1:0).", ".($tiny ? "CURRENT_TIMESTAMP + INTERVAL 20 MINUTE":"'%a2'").", ".$usages.");";
		$DBManager->insert($sql,$key,$url,$expiryDate);
	} else {
		$sql = "INSERT INTO `entries` (`id`, `url`, `protected`, `valid_until`, `usages`) VALUES ('%a0', '%a1', ".($protected?1:0).", ".($tiny ? "CURRENT_TIMESTAMP + INTERVAL 20 MINUTE":"NULL").", ".$usages.");";
		$DBManager->insert($sql,$key,$url);
	}
	return_data(array("link_ID"=>$key),201);
} catch (mysqli_sql_exception $e) {
	error_log($e->getMessage().var_export($e->getTraceAsString(), true));
	return_error("Error while communicating with the database.");
}

function createKey($len) {return substr(md5(random_bytes(100)),0,$len);}
function curl($url,$auth){
	$req = curl_init($url);
	curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
	if(isset($auth)&&$auth!=null) curl_setopt($req, CURLOPT_HTTPHEADER, array("x-apikey: ".$auth));
	$out = curl_exec($req);
	if(($http_code= curl_getinfo($req, CURLINFO_HTTP_CODE))!=200) {
        return_error("Error requesting API: " . $http_code);
	}
	curl_close($req);
	$array_resp = json_decode($out,true);
	if(isset($array_resp["error"])) {
        return_error($array_resp["error"]);
	}
	return $array_resp;
}
?>