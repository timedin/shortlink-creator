(function(w, d, apiEndpoint,shortEndpoint, _qs) {
    "use strict";
    var urlF = _qs("input#urlInput");
    var sendBtn = _qs("button#sendBtn");
    var protCb = _qs("input#protectedInput");
    var outLink = _qs("input#shortlinkOut");
    var tinyLink = _qs("input#tinyInput");
    var expiryDate = _qs("input#dateInput");
    var usagesInput = _qs("input#usagesInput");
    var disabled = true;
    checkDOM();
    urlF.addEventListener("input", function(e) {
        checkDOM();
        var url = urlF.value;
        var r = new RegExp(/^(|http|https):\/\/[^ "]+\...*$/);
        disabled = sendBtn.disabled = !r.test(url);
    });
    usagesInput === null || usagesInput === void 0 ? void 0 : usagesInput.addEventListener("change", function(e) {
        disabled = sendBtn.disabled = (usagesInput.value < 0);
    });
    tinyLink === null || tinyLink === void 0 ? void 0 : tinyLink.addEventListener("change", function(e) {
        var tiny = tinyLink.checked;
        expiryDate.disabled = tiny;
    });
    sendBtn.addEventListener("click", function(e) {
        if (disabled)
            return;
        checkDOM();
        var data = {
            url: urlF.value,
            protected: protCb.checked,
            tiny: tinyLink.checked,
            usages: usagesInput.value
        };
        if (!tinyLink.checked && expiryDate.value != "") {
            data["expire_date"] = expiryDate.value.replace("T", " ") + ":00";
        }
        w["pRJ"](apiEndpoint, JSON.stringify(data), function(res) {
            var json = JSON.parse(res.responseText);
            if (json.state != "success") {
                alert("Error communicating to Server: " + json.description);
                return;
            }
            outLink.value = (shortEndpoint + json.link_ID);
        });
    });

    function checkDOM() {
        if (urlF == null || sendBtn == null || outLink == null || protCb == null) {
            throw new Error("Critical: Invalid DOM detected, missing Elements");
        }
    }
})(window, document, window["apiEndpoint"], window["shortEndpoint"], function(s) {return document.querySelector(s);});
const rState = {
    success: 'success',
    error: 'error'
};
var pRJ = (u, v, c) => {
    var r = new XMLHttpRequest();
    r.open('POST', u, true);
    r.setRequestHeader('Content-type', 'application/json');
    r.onload = function() {
        c(this)
    };
    r.send(v);
};