<?php
require_once(__DIR__."/../../res/setup.php")
?>
<script defer src="./script.js"></script>
<label for="urlInput">URL:</label><input required minlength="10" type="text" id="urlInput" placeholder="https://"><br>
<button id="sendBtn" disabled>Create</button><br><br>
<hr>
<label for="protectedInput">Protected (Captcha):</label><input type="checkbox" id="protectedInput"><br>
<label for="tinyInput">Tiny Link (20min valid):</label><input type="checkbox" id="tinyInput"><br>
<label for="dateInput">Expiry Date:</label><input type="datetime-local" id="dateInput"><br>
<label for="usagesInput">Limited Usages:</label><input type="number" id="usagesInput" value="0" min="0">
<label>Short-Link:</label><input type="text" readonly id="shortlinkOut">
<script>
    var apiEndpoint = "<?php echo $config["api_endpoint"]; ?>";
    var shortEndpoint = "<?php echo $config["short_endpoint"]; ?>";
</script>