# Shortlink Creator
The source code for the timedin.net [Shortlink-System](https://www.timedin.net/tools/shortlink/).

A small shortlink system with some advanced feautures
## Configuration

Before using the script, you need to configure the following parameters in the script `/config/config.ini`, use `/config/config.sample.ini` as reference:

- database:`user`, `passwd` and `database` for access to your database
- config: `length` for length of normal shortlinks (characters after slash), `tiny-length` length of tiny (20min) shortlinks
- scan-vt: `enabled` de/activate VirusTotal scans, `token` your api token, `threshold` the number of sources needed to detect as malicious

### HCaptcha (Verification)
- Go to [hCaptcha.com](https://hcaptcha.com) and create a token
- paste the token in `$hcaptcha_token = "HERE"` in `redirector/redirector.php`
- paste the site key in `data-sitekey="HERE"` in `redirector/verification.php`
### Database Connection (for `redirector/redirector.php` and `creator/api.php)
- Download the timedin PHP-DBManager and paste it's path to `$db_connector_path="HERE"`
- Create an .ini file with the credentials for the database, paste its path to `$db_cred_file = "HERE"`
- Setup the Database with the setup file (coming...)
### API-Script
- In `creator/frontend/script.js` set the url of the API-endpoint `var apiPath = "HERE";`
### Server Setup
- Use the htaccess (or server configuration) to internally redirect every request to the redirector server to `redirector/redirector.php`
- Disallow access: 
	- `redirector/404.php`
	- `redirector/cleanup.php`
	- `redirector/verification.php`
	- `creator/backend/json-api-common.php`